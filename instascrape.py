
import re
import json
import requests
import sys 
import os
from time import sleep
import sys

class instascrape:

	def __init__(self,username):
		
		self.username=username
		self.max_id=None
		self.post_num=0

	def crawl(self):
		"""Crawls and downloads the images """

		if self.max_id is None:
			url = 'http://instagram.com/{}/media'.format(self.username)
		else:
			url='http://instagram.com/{}/media/?&max_id={}'.format(self.username,self.max_id)
		
		try:	
			resp=requests.get(url)
		except requests.exceptions.ConnectionError:
			sleep(5)
			resp=requests.get(url)

		data=json.loads(resp.text)
		self.download(data)
		
		if 'more_available' in data and data['more_available'] is True:
			self.max_id=data['items'][-1]['id']
			self.crawl()


	def download(self,data):

		"""Sets directory and filenames and downloads files """

		try:
			os.makedirs(self.username)
		except:
			pass
		
		for values in data['items']:
			url_temp=values["images"]['standard_resolution']['url']
			with open('{}/{}.jpg'.format(self.username,self.post_num), 'wb') as f:
				
				try:
					bytes = requests.get(url_temp).content
				except requests.exceptions.ConnectionError:
					sleep(5)
					bytes = requests.get(url_temp).content
				
				f.write(bytes)
				sys.stdout.write('\rDownloaded {} images'.format(self.post_num))
				sys.stdout.flush()

		self.post_num=self.post_num+1


		

		

		

if __name__ == '__main__':

	username =sys.argv[1]
	scraper=instascrape(username)
	scraper.crawl()